var express = require('express');
var router = express.Router();


var mongoose = require("mongoose"),
  Schema = mongoose.Schema;


var albums = require('../core_modules/albums');



router.get('/:page', function(req, res, next) {
  var query = albums.find( {}, {album_name : 1} );
  query.skip((req.params.page-1)*5);
  query.limit(5);
  query.exec(function(err,data){
    res.send(data);
  });

});



module.exports = router;
