var express = require('express');
var router = express.Router();


var mongoose = require("mongoose"),
	Schema = mongoose.Schema;


var albums = require('../core_modules/albums');

router.get('/page/:page', function(req, res, next) {

	var query = albums.find( {}, {album_name : 1} );
	query.skip((req.params.page-1)*5);
	query.limit(5);
	query.exec(function(err,data){
		// console.log(data);
		res.send(data);
	});

});

router.get('/:id', function(req, res, next) {
	console.log(req.params.id);
	var query = albums.find( {_id :req.params.id});
	query.exec(function(err,data){
		console.log(data);
		res.send(data);
	});

});

module.exports = router;
