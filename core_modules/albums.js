var mongoose = require("mongoose"),
	Schema = mongoose.Schema;



var albumSchema = mongoose.Schema({
		album_name : String,
		album_artist : String,
		album_rating : Number,
		release_date : Date,
		songs : [{
			title : String,
			path : String,
			rating : String,
			size : String,
			duration : String,
			bit_rate : String
		}],
		comments : [{
			name : String,
			description : String,
			email : String,
			time : String
		}]
});


module.exports = function(){
	// Return the Caterer Model
	return mongoose.model('albums', albumSchema);
}();