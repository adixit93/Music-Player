angular.module('musicApp',['ngRoute'])
	.controller("MainCtrl",["$http","$scope",function($http,$scope){

		$scope.$on('$viewContentLoaded', function(){
			$(".button-collapse").sideNav({
	            menuWidth: 250, // Default is 240
	            edge: 'left', // Choose the horizontal origin
	            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
	          });
	         $('.collapsible').collapsible();
		});

	}])
	.directive('navBar', function() {
		return {
			restrict: 'E',
	    	templateUrl: '/views/elements/nav-bar.html',
	  };
	})
	.directive('sideBar', function() {
		return {
			restrict: 'E',
	  	 	templateUrl: '/views/elements/side-bar.html',
	  };
	});


