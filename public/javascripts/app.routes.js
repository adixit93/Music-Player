angular.module('musicApp')
.config(function ($routeProvider) {

	$routeProvider

	.when('/TopReleases', {

		templateUrl: '/views/pages/topreleases/topreleases.html',
		controller: "TopReleasesController"
	})

	.when('/Albums/pages', {
		templateUrl: '/views/pages/album/album.html',
		controller: "AlbumController"
	})

	.when('/NewReleases', {
		templateUrl: '/views/pages/newreleases/newreleases.html',
		controller: "NewReleasesController"
	})

	.when('/Albums/:id', {
		templateUrl: '/views/pages/album/albumDetails.html',
		controller: "AlbumDetailController"
	})
	
	// .otherwise({ redirectTo: '/TopReleases' })
});

